﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Moli.Services
{
    public static class Permissions
    {
        // Change all Permission URI to their friendly name when passing to the UI.
        // We don't want to pass to the UI the actual URI values.
        // And, we don't want the UI giving us actual URI values.
        // A user could craft custom Permission URIs, and that wouldn't be good.


        public static class User
        {
            private const String BaseUriFormat = "user/{0}";


            #region // Permission URI methods
            // ==================================================
            public readonly static Func<String> ViewDashboard =
                () => String.Format(BaseUriFormat, "view-dashboard");
            public readonly static Func<String> AdminLocations =
                () => String.Format(BaseUriFormat, "admin-locations");
            public readonly static Func<String> AdminLocationZones =
                () => String.Format(BaseUriFormat, "admin-locationzones");
            #endregion


            #region // Special Permission URI methods
            // ==================================================
            public readonly static Func<String> Developer =
                () => String.Format(BaseUriFormat, "*");
            public readonly static Func<String> DeveloperFlag =
                () => String.Format(BaseUriFormat, "dev");
            public readonly static Func<String> Diagnostics =
                () => String.Format(BaseUriFormat, "diagnostics");

            public readonly static Func<String> HasAny =
                () => String.Format(BaseUriFormat, "*");
            // ==================================================
            #endregion


            #region // Friendly Permission dictionaries
            // ==================================================
            public const String FriendlyViewDashboard = "Dashboard";
            public const String FriendlyAdminLocations = "Locations";
            public const String FriendlyAdminLocationZones = "Location Zones";
            public const String FriendlyDeveloper = "Developer";

            public readonly static Dictionary<String, Func<String>> All = new Dictionary<String, Func<String>>
            {
                { FriendlyViewDashboard, ViewDashboard },
                { FriendlyAdminLocations, AdminLocations },
                { FriendlyAdminLocationZones, AdminLocationZones }
            };
            private readonly static Dictionary<String, Func<String>> AllWithSpecial = new[]
            {
                // Concatenate all dictionaries
                All,
                new Dictionary<String, Func<String>>
                {
                    // Special permissions, not selectable through the UI
                    { FriendlyDeveloper, Developer }
                }
            }
            .SelectMany(d => d)
            .ToDictionary(p => p.Key, p => p.Value);
            // ==================================================
            #endregion


            #region // Friendly <-> URI Converter methods
            // ==================================================
            public readonly static Func<String, String> FriendlyToUri =
                name => AllWithSpecial[name]();
            public readonly static Func<String, String> UriToFriendly =
                uri => AllWithSpecial.FirstOrDefault(p => String.Equals(p.Value(), uri)).Key;
            // ==================================================
            #endregion
        }
    }
}