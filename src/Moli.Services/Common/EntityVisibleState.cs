﻿namespace Moli.Services.Common
{
    public enum EntityVisibleState : byte
    {
        All,
        NonVisibleOnly,
        VisibleOnly
    }
}