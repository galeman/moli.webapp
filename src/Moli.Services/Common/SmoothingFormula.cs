﻿namespace Moli.Services.Common
{
    public enum SmoothingFormula : byte
    {
        None,
        SimpleMovingAverage,
        ExponentialMovingAverage,
        Median,
        TakacsTimeBased,
        TakacsPointsBased,
        TakacsTimeBasedWithAverage,
        TakacsPointsBasedWithAverage,
    }
}