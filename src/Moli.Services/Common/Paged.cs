﻿using System.Collections.Generic;
using System.Linq;

namespace Moli.Services.Common
{
    public sealed class Paged<T>
    {
        public Paged(IEnumerable<T> data, long total)
        {
            Data = data.ToList();
            Total = total;
        }

        public IList<T> Data { get; set; }
        public long Total { get; set; }
    }
}