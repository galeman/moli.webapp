﻿namespace Moli.Services.Common
{
    public enum OriginLocation : byte
    {
        TopLeft,
        TopRight,
        BottomRight,
        BottomLeft
    }
}