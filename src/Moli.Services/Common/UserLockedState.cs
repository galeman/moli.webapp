﻿namespace Moli.Services.Common
{
    public enum UserLockedState
    {
        NotLockedOnly =0,
        LockedOnly = 1,
        All =2,
    }
}
