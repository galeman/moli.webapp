﻿namespace Moli.Services.Common
{
    public enum EntityCancelledState : byte
    {
        All,
        NonCancelledOnly,
        CancelledOnly
    }
}