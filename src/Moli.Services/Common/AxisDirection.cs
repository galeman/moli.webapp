﻿namespace Moli.Services.Common
{
    public enum AxisDirection : byte
    {
        Normal,
        Inverted
    }
}