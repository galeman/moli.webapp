﻿namespace Moli.Services.Common
{
    public enum EntitySubmittedState : byte
    {
        All,
        NonSubmittedOnly,
        SubmittedOnly
    }
}