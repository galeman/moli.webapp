﻿using Moli.Services.Event;
using Moli.Services.Infrastructure;
using Moli.Services.Location;
using Moli.Services.LocationZone;
using Moli.Services.User;
using System;

namespace Moli.Services
{
    public static class Repositories
    {
        public static readonly Lazy<EventRepository> ForEvents =
           new Lazy<EventRepository>(() => new EventRepository(Client.ViewStore()));
        public static readonly Lazy<UserRepository> ForUsers =
            new Lazy<UserRepository>(() => new UserRepository(Client.ViewStore()));
         public static readonly Lazy<LocationRepository> ForLocations =
            new Lazy<LocationRepository>(() => new LocationRepository(Client.ViewStore()));
        public static readonly Lazy<LocationZoneRepository> ForLocationZones =
            new Lazy<LocationZoneRepository>(() => new LocationZoneRepository(Client.ViewStore()));
    }
}