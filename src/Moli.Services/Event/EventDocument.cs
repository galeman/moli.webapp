﻿using Moli.Services.Common;
using System;

namespace Moli.Services.Event
{
    public sealed class EventDocument
    {
        public DateTimeOffset Timestamp { get; set; }
        public EventSource EventSource { get; set; }
        public String EventName { get; set; }
        public String Message { get; set; }
    }
}