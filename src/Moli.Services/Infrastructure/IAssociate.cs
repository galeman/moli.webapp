﻿namespace Moli.Services.Infrastructure
{
    public interface IAssociate<in T>
    {
        void Associate(T value);
    }
}