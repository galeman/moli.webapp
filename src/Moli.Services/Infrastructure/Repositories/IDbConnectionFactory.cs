﻿using System.Data.Common;

namespace Moli.Services.Infrastructure.Repositories
{
    public interface IDbConnectionFactory
    {
        DbConnection OpenDbConnection();
        DbConnection CreateDbConnection();
    }
}