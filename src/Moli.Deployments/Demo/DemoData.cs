﻿using Moli.Services;

namespace Moli.Deployments.Demo
{
    public static class DemoData
    {
        public static void Setup()
        {
            var developer = Helper.CreateUser(
                "franwell-dev",
                "Franwell Developers",
                "Jesse.Naranjo@Franwell.com",
                new[] { Permissions.User.FriendlyDeveloper },
                0,
                false);
            Helper.CreateUser(
                "demo",
                "Demo User",
                "Moli@Franwell.com",
                new[] { Permissions.User.FriendlyViewDashboard },
                developer.Id);
        }
    }
}