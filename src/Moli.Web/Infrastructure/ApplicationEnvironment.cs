﻿using System;
using System.Web;

namespace Moli.Web.Infrastructure
{
    public static class ApplicationEnvironment
    {
        private static Boolean _IsRunningProduction = true; // Always assume Production


        public static Boolean IsRunningProduction
        {
            get { return _IsRunningProduction; }
        }

        public static Boolean IsUsingHttps
        {
            get { return HttpContext.Current != null && HttpContext.Current.Request.IsSecureConnection; }
        }

        public static void VerifyProduction()
        {
            var host = HttpContext.Current.Request.Url.Host;

            _IsRunningProduction =
                String.Equals(host, "ac.moli.com", StringComparison.OrdinalIgnoreCase) ||
                ConnectionStrings.IsProduction;
        }
    }
}