﻿using System;

namespace Moli.Web.Infrastructure
{
    public static class Constants
    {
        public const String AntiForgeryCookieName = "MoliRequestToken";
        public const String TimeoutCookieName = "MoliTimeout";
    }
}