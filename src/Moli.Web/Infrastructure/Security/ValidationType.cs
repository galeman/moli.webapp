﻿namespace Moli.Web.Infrastructure.Security
{
    public enum ValidationType
    {
        Key,
        Passcode,
        Password,
        Username,
    }
}