﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace Moli.Web.Infrastructure
{
    public static class ApplicationMetadata
    {
        public static String Version
        {
            get { return FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion; }
        }

        public static String BuildString
        {
            get { return FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion; }
        }
    }
}