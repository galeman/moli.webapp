﻿using System;
using System.Web.Mvc;

namespace Moli.Web.Infrastructure.Extensions
{
    public static class UrlHelperExtensions
    {
        public static String ActionApi(this UrlHelper url, String actionName, String controllerName)
        {
            return url.Action(actionName, controllerName, new { httproute = "" });
        }
    }
}