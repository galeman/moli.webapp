﻿// franwell.format.js
// (c) 2012-2015 Franwell, Inc.

// requires - kendo.web.min.js, moment.min.js

(function () {
    'use strict';


    var root = window,
        franwell = root.franwell = root.franwell || {};


    // Origin: http://stackoverflow.com/a/12034334/238722
    var escapeHtmlEntityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        "/": '&#x2F;'
    };

    // =========================================================================
    // escapeHtml : Ensures that the input text can be safely outputted as HTML
    // -------------------------------------------------------------------------
    //    This function is really only necessary when NOT using any other
    //    formatting function from this source file, AND when passing
    //    user-input directly into innerHTML, or jQuery's html(...) function
    // =========================================================================

    franwell.escapeHtml = franwell.escapeHtml || function (text) {
        if (!text) {
            return '';
        }

        var result = String(text).replace(/[&<>"'\/]/g, function (s) {
            return escapeHtmlEntityMap[s];
        });
        return result;
    };


    // =============================================================================
    // isNull : Returns the specified default value, if value is not defined or null
    // =============================================================================

    franwell.isNull = franwell.isNull || function (value, defaultValue) {
        var result = franwell.escapeHtml(value) ||
            (defaultValue != null ? defaultValue : '<span class="muted">N/A</span>');
        return result;
    };


    // ===================================================================================
    // composeAddress : Formats an AddressDocument into HTML with <br /> between each line
    // ===================================================================================

    franwell.composeAddress = franwell.composeAddress || function (addressDocument, fieldNames, lineSeparator) {
        if (!addressDocument) {
            return franwell.isNull();
        }
        fieldNames = $.extend(true, {
            recipient: 'Recipient',
            street1: 'Street1',
            street2: 'Street2',
            street3: 'Street3',
            street4: 'Street4',
            city: 'City',
            state: 'State',
            zipCode: 'PostalCode'
        }, fieldNames || {});
        if (!lineSeparator) lineSeparator = '<br />';


        var result = '';

        if (addressDocument[fieldNames.recipient] && addressDocument[fieldNames.recipient] !== '') {
            result += franwell.escapeHtml(addressDocument[fieldNames.recipient]) + lineSeparator;
        }

        if (addressDocument[fieldNames.street1] && addressDocument[fieldNames.street1] !== '') {
            result += franwell.escapeHtml(addressDocument[fieldNames.street1]) + lineSeparator;
        }
        if (addressDocument[fieldNames.street2] && addressDocument[fieldNames.street2] !== '') {
            result += franwell.escapeHtml(addressDocument[fieldNames.street2]) + lineSeparator;
        }
        if (addressDocument[fieldNames.street3] && addressDocument[fieldNames.street3] !== '') {
            result += franwell.escapeHtml(addressDocument[fieldNames.street3]) + lineSeparator;
        }
        if (addressDocument[fieldNames.street4] && addressDocument[fieldNames.street4] !== '') {
            result += franwell.escapeHtml(addressDocument[fieldNames.street4]) + lineSeparator;
        }

        if (addressDocument[fieldNames.city] && addressDocument[fieldNames.city] !== '') {
            result += franwell.escapeHtml(addressDocument[fieldNames.city]);
        }
        if (addressDocument[fieldNames.state] && addressDocument[fieldNames.state] !== '') {
            if (addressDocument[fieldNames.city] && addressDocument[fieldNames.city] !== '') {
                result += ', ';
            }
            result += franwell.escapeHtml(addressDocument[fieldNames.state]);
        }
        if (addressDocument[fieldNames.zipCode] && addressDocument[fieldNames.zipCode] !== '') {
            if ((addressDocument[fieldNames.state] && addressDocument[fieldNames.state] !== '') ||
                (addressDocument[fieldNames.city] && addressDocument[fieldNames.city] !== '')) {
                result += ' ';
            }
            result += franwell.escapeHtml(addressDocument[fieldNames.zipCode]);
        }

        if (result === '') {
            result = franwell.isNull();
        }
        return result;
    };


    // =====================================
    // formatDate : Formats a date to String
    // =====================================

    franwell.formatDate = franwell.formatDate || function (date) {
        if (!date) {
            return '';
        }

        var result = moment(date);
        return result.format('MM/DD/YYYY');
    };


    // ================================================================================
    // formatDateTime : Formats a date/time to String
    // --------------------------------------------------------------------------------
    //  - multiline: breaks the text into two lines: one for the date, one for the time
    //  - includeSeconds: include the seconds portion of the time
    //  - includeMilliseconds: include the milliseconds portion of the time
    //  - asUtc: maintains the date as UTC, instead of converting to the local timezone
    // ================================================================================

    franwell.formatDateTime = franwell.formatDateTime || function (dateTime, multiline, includeSeconds, includeMilliseconds, asUtc) {
        if (!dateTime) {
            return '';
        }

        if (multiline == null) multiline = false;
        if (includeSeconds == null) includeSeconds = false;
        if (includeMilliseconds == null) includeMilliseconds = false;
        if (asUtc == null) asUtc = false;

        var result = (asUtc ? moment.utc(dateTime) : moment(dateTime));
        return result.format(
            'MM/DD/YYYY' +
            (multiline ? '<br />' : ' ') +
            'hh:mm' + (includeSeconds ? ':ss' : '') +
            (includeMilliseconds ? '.SSS' : '') +
            ' a');
    };


    // ===========================================================================
    // formatTimeFromNow : Formats a date/time as a String representation from now
    // ===========================================================================

    franwell.formatTimeFromNow = franwell.formatTimeFromNow || function (dateTime) {
        if (!dateTime) {
            return '';
        }

        var result = moment(dateTime);
        return result.fromNow();
    };


    // ==========================================================================================================================
    // formatNumber : Formats a number to String
    // --------------------------------------------------------------------------------------------------------------------------
    //  - decimalPlaces: number of decimal places to display
    //  - thousands: whether to display a thousands separator, or not
    //  - zeroAsDecimalPlaceholder: if true, will display 0, otherwise will display up to the number of "decimalPlaces" specified
    // ==========================================================================================================================

    franwell.formatNumber = franwell.formatNumber || function (number, decimalPlaces, thousands, zeroAsDecimalPlaceholder) {
        if (number !== 0 && !number) {
            return franwell.isNull();
        }

        if (decimalPlaces == null) decimalPlaces = 0;
        if (thousands == null) thousands = true;
        if (zeroAsDecimalPlaceholder == null) zeroAsDecimalPlaceholder = false;

        var stringFormat = (thousands ? '\#\#,0' : '0');
        if (typeof decimalPlaces === 'number' && decimalPlaces > 0) {
            stringFormat += '.';
            for (var i = 0; i < decimalPlaces; i++) {
                stringFormat += (zeroAsDecimalPlaceholder ? '0' : '#');
            }
        }

        var escapedNumber = Number(franwell.escapeHtml(number));
        var result = kendo.toString(escapedNumber, stringFormat);
        return result;
    };


    // ========================================================================================================
    // formatCurrency : Formats a number as currency, with a dollar sign, thousands separator, and two decimals
    // ========================================================================================================

    franwell.formatCurrency = franwell.formatCurrency || function (number) {
        if (number !== 0 && !number) {
            return franwell.isNull();
        }

        var escapedNumber = Number(franwell.escapeHtml(number));
        var result = kendo.toString(escapedNumber, 'c');
        return result;
    };


    // =========================================================================================
    // formatQuantity : Formats a number with thousands separator, and up to four decimal places
    // =========================================================================================

    franwell.formatQuantity = franwell.formatQuantity || function (number) {
        if (number !== 0 && !number) {
            return franwell.isNull();
        }

        var escapedNumber = Number(franwell.escapeHtml(number));
        var result = kendo.toString(escapedNumber, '\#\#,0.\#\#\#\#');
        return result;
    };


    // ==================================================
    // formatPoints : Formats an array of Point3D as text
    // ==================================================

    franwell.formatPoints = franwell.formatPoints || function (points) {
        console.log(points);
        if (!(points instanceof Array)) {
            return franwell.isNull();
        }

        var result = '',
            icnt = points.length;

        if (icnt !== 0) {
            var i,
                point = points[0];

            result =
                '(' +
                    franwell.formatNumber(point.X, 2, false) +
                    ',' +
                    franwell.formatNumber(point.Y, 2, false) +
                ')';
            for (i = 1; i < icnt; i++) {
                point = points[i];
                result =
                    ' (' +
                        franwell.formatNumber(point.X, 2, false) +
                        ',' +
                        franwell.formatNumber(point.Y, 2, false) +
                    ')';
            }
        }
        return result;
    };


    // ==============================================================================================================
    // formatGridColumnsAsObjectArray : Formats a grid column array into an array of objects { title: '', value: '' }
    // ==============================================================================================================

    franwell.formatGridColumnsAsObjectArray = franwell.formatGridColumnsAsObjectArray || function (entity, columns, columnsToSkip, columnsWithHtml) {
        var result = [];

        if (!entity || !columns || !(columns instanceof Array) || columns.length === 0) {
            return result;
        }

        if (!columnsToSkip || !(columnsToSkip instanceof Array)) columnsToSkip = [];
        if (!columnsWithHtml || !(columnsWithHtml instanceof Array)) columnsWithHtml = [];

        var column,
            columnValue,
            columnContainsHtml;

        for (var i = 0, icnt = columns.length; i < icnt; i++) {
            column = columns[i];

            if (columnsToSkip.length !== 0 && columnsToSkip.indexOf(column.field) !== -1) {
                continue;
            }
            if (column.template) {
                columnValue = kendo.template(column.template)(entity);
            } else {
                columnValue = franwell.getRecursiveProperty(entity, column.field);
            }
            columnContainsHtml = columnsWithHtml.length !== 0 && columnsWithHtml.indexOf(column.field) !== -1;

            result.push({
                title: franwell.escapeHtml(column.title != null ? column.title : column.field),
                value: (columnValue != null && columnValue !== ''
                    ? (columnContainsHtml ? columnValue : franwell.escapeHtml(columnValue))
                    : franwell.isNull())
            });
        }

        return result;
    };


    // ====================================================================
    // formatGridColumnsAsHtml : Formats a grid column array in HTML markup
    // ====================================================================

    franwell.formatGridColumnsAsHtml = franwell.formatGridColumnsAsHtml || function (entity, columns, columnsToSkip, columnsWithHtml, horizontal) {
        var objectsArray = franwell.formatGridColumnsAsObjectArray(entity, columns, columnsToSkip, columnsWithHtml);
        if (objectsArray.length === 0) {
            return '';
        }

        if (horizontal == null) horizontal = true;

        var result = '<dl' + (horizontal ? ' class="dl-horizontal"' : '') + '>';
        objectsArray.forEach(function (obj) {
            result +=
                '<dt>' + obj.title + '</dt>' +
                '<dd>' + obj.value + '</dd>';
        });
        result += '</dl>';

        return result;
    };


    // ============================================================================================
    // formatGridColumnsAsText : Formats a grid column array as series of text lines "title: value"
    // ============================================================================================

    franwell.formatGridColumnsAsText = franwell.formatGridColumnsAsText || function (entity, columns, columnsToSkip, columnsWithHtml) {
        var objectsArray = franwell.formatGridColumnsAsObjectArray(entity, columns, columnsToSkip, columnsWithHtml);
        if (objectsArray.length === 0) {
            return '';
        }

        var result = '';
        objectsArray.forEach(function (obj) {
            result +=
                obj.title + ': ' +
                $('<div>' + obj.value + '</div>').text() + '\n';
        });
        result = result.replace(/\n+$/g, '');

        return result;
    };
})();