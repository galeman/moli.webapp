﻿// franwell.kendo.js
// (c) 2012-2014 Franwell, Inc.

// requires franwell.js, franwell.kendo.js, franwell.format.js

(function () {
    'use strict';


    var root = window,
        franwell = root.franwell = root.franwell || {};
    franwell.$ = franwell.$ || root.jQuery;
    franwell.entitySelector = franwell.entitySelector || {};


    var initializeWindow = function () {
        if (franwell.entitySelector.kendoWindow) return;

        var $entitySelector = franwell.$('#entity_selector'),
            $currentLineTitle = $entitySelector.children('#entityselector-currentline-title'),
            $currentLineGrid = $entitySelector.children('#entityselector-currentline-grid'),
            $noduplicatesTitle = $entitySelector.children('#entityselector-noduplicates-title'),
            $availableGrid = $entitySelector.children('#entityselector-available-grid'),
            $okButton = $entitySelector.find('#entityselector-ok-button'),
            $cancelButton = $entitySelector.find('#entityselector-cancel-button');

        //franwell.entitySelector.windowElement = $entitySelector;
        franwell.entitySelector.currentLineTitle = $currentLineTitle;
        franwell.entitySelector.currentLineGridElement = $currentLineGrid;
        franwell.entitySelector.noduplicatesTitle = $noduplicatesTitle;
        franwell.entitySelector.availableGridElement = $availableGrid;
        franwell.entitySelector.okButton = $okButton;
        //franwell.entitySelector.cancelButton = $cancelButton;

        $entitySelector.kendoWindow({
            // config
            actions: ['Close'],
            draggable: true,
            modal: true,
            resizable: false,
            title: 'Select'
        });
        franwell.entitySelector.kendoWindow = $entitySelector.data("kendoWindow");

        $cancelButton.click(function () {
            franwell.entitySelector.kendoWindow.close();
        });
    },

    showWindow = function (options) {
        initializeWindow();

        var $currentLineTitle = franwell.entitySelector.currentLineTitle,
            $currentLineGrid = franwell.entitySelector.currentLineGridElement,
            $noduplicatesTitle = franwell.entitySelector.noduplicatesTitle,
            $availableGrid = franwell.entitySelector.availableGridElement,
            $okButton = franwell.entitySelector.okButton,
            entities = options.entities.slice();

        if (franwell.entitySelector.kendoGridCurrentLine) {
            franwell.entitySelector.kendoGridCurrentLine.destroy();
            franwell.entitySelector.kendoGridCurrentLine = null;
            $currentLineGrid.empty();
        }
        if (franwell.entitySelector.kendoGridAvailable) {
            franwell.entitySelector.kendoGridAvailable.destroy();
            franwell.entitySelector.kendoGridAvailable = null;
            $availableGrid.empty();
        }

        if (options.selectedIds.length !== 0 && options.selectedIdField) {
            var i, icnt, x, xcnt,
                currentLineData = [],
                selectedObject, selectedId, usedCount, entityObject, addingCount;

            for (i = 0, icnt = options.selectedIds.length; i < icnt; i++) {
                selectedObject = options.selectedIds[i];

                selectedId = selectedObject.id;
                if (selectedId == null) continue;

                usedCount = selectedObject.count;
                if (usedCount == null || usedCount === 0) continue;

                addingCount = false;

                x = 0;
                xcnt = entities.length;
                while (x < xcnt) {
                    entityObject = $.extend(true, {}, entities[x]);
                    if (selectedId !== entityObject[options.selectedIdField] && !addingCount) {
                        x++;
                        continue;
                    }

                    if (options.allowDuplicates) {
                        x++;
                    } else {
                        entities.splice(x, 1);
                        xcnt = entities.length;
                    }

                    entityObject.RowNumber = selectedObject.rowNumber;
                    currentLineData.push(entityObject);

                    usedCount--;
                    if (usedCount === 0) {
                        break;
                    } else if (!addingCount) {
                        addingCount = true;
                    }
                }
            }

            if (currentLineData.length !== 0) {
                $currentLineTitle.show();
                $currentLineGrid.show();

                var extendedColumns = options.columns.slice(),
                    currentLineDataSource = franwell.kendo.clientDataSource({
                        data: currentLineData,
                        model: options.model,
                        pageSize: 5,
                        sort: { field: 'RowNumber', dir: 'asc' }
                    });

                extendedColumns.splice(0, 0, {
                    title: 'Line', field: 'RowNumber'
                });

                $currentLineGrid.franwellGrid({
                    dataSource: currentLineDataSource,
                    columns: extendedColumns,
                    filterable: false,
                    pageable: currentLineData.length > 5 ? { refresh: false } : false,
                    selectable: false,
                    sortable: false
                });
                currentLineDataSource.read();
                franwell.entitySelector.kendoGridCurrentLine = $currentLineGrid.data('kendoGrid');
            } else {
                $currentLineTitle.hide();
                $currentLineGrid.hide();
            }
        } else {
            $currentLineTitle.hide();
            $currentLineGrid.hide();
        }

        if (options.allowDuplicates) {
            $noduplicatesTitle.hide();
        } else {
            $noduplicatesTitle.show();
        }

        var availableDataSource = franwell.kendo.clientDataSource({
            data: entities,
            model: options.model,
            pageSize: 10
        });
        $availableGrid.franwellGrid({
            dataSource: availableDataSource,
            columns: options.columns,
            pageable: entities.length > 10 ? { refresh: false } : false,
            //selectable: options.multiselect === false
            //    ? 'row'
            //    : 'multiple, row'
            selectable: 'row'
        });
        availableDataSource.read();
        franwell.entitySelector.kendoGridAvailable = $availableGrid.data('kendoGrid');


        $okButton
            .text('Select on Line # ' + (Number(options.selectedIndex) + 1).toString())
            .off('click')
            .click(function () {
                if (typeof options.onOkCallback === 'function') {
                    var ids = $availableGrid.franwellGridIds(options.selectedIdField);
                    if (ids.length === 0) {
                        if (!confirm('Proceeding with no selection will cause the currently selected item (if any) to be unselected.\nContinue anyway?')) {
                            return;
                        }
                    }
                    options.onOkCallback(ids);
                }

                franwell.entitySelector.kendoWindow.close();
            });

        franwell.entitySelector.kendoWindow.center();
        franwell.entitySelector.kendoWindow.open();
    },

    getSelectedObjects = function (options) {
        var result = [],
            i, icnt;

        if (options.subLists == null || options.subListIndex == options.subLists.length) {
            var listObject, usedRange, rowNumber;

            for (i = 0, icnt = options.list.length; i < icnt; i++) {
                listObject = options.list[i];

                usedRange = options.usedRangeField
                    ? listObject[options.usedRangeField]
                    : 1;
                if (typeof usedRange !== 'number') usedRange = 1;

                rowNumber = '';
                if (options.parentListIndex != null) {
                    rowNumber = (options.parentListIndex + 1).toString() + '.';
                }
                rowNumber += (i + 1).toString();

                result.push({
                    id: listObject[options.idField],
                    count: usedRange,
                    rowNumber: rowNumber
                });
            }
        } else {
            var subListPropertyName = options.subLists[options.subListIndex + 1],
                nextSublistIndex = options.subListIndex + 2;

            for (i = 0, icnt = options.list.length; i < icnt; i++) {
                result.push.apply(
                    result,
                    getSelectedObjects({
                        idField: options.idField,
                        usedRangeField: options.usedRangeField,
                        list: options.list[i][subListPropertyName],
                        subLists: options.subLists,
                        subListIndex: nextSublistIndex,
                        parentListIndex: i
                    }));
            }
        }

        return result;
    },

    setSelectedObjects = function (options) {
        if (options.subLists == null || options.subListIndex == options.subLists.length) {
            options.list[options.index][options.idField] = options.ids[0];
        } else {
            var listIndex = Number(options.subLists[options.subListIndex]),
                subListPropertyName = options.subLists[options.subListIndex + 1],
                nextSublistIndex = options.subListIndex + 2;

            setSelectedObjects({
                idField: options.idField,
                index: options.index,
                list: options.list[listIndex][subListPropertyName],
                ids: options.ids,
                subLists: options.subLists,
                subListIndex: nextSublistIndex
            });
        }
    },

    getAngularJsValidationFunction = function ($this) {
        var $validatedSibling = $this.siblings('.js-validated-element:first'),
            siblingValidator = $this.attr('data-sibling-validator'),
            angularJsCode = siblingValidator ? $validatedSibling.attr(siblingValidator) : null;
        if ($validatedSibling.length === 0 || !siblingValidator || !angularJsCode) return null;
        return angularJsCode
            .replace(/validateSelection/g, '$scope.validateSelection')
            .replace(/([\(, ]+)preload\./g, '$1window.preload.');
    };


    franwell.entitySelector.getSearchClickHandler = franwell.entitySelector.getSearchClickHandler || function (options) {
        return function () {
            var me = this,
                $this = $(me),
                idField = $this.attr('data-id-field').split(','),
                usedRangeField = $this.attr('data-line-usedrange'),
                index = Number($this.attr('data-index')),
                detailsList = options.detailsList,
                detailsSubLists = $this.attr('data-sublists'),
                lineIdField, entityIdField, selectedIds,
                validationFunction = getAngularJsValidationFunction($this);

            entityIdField = idField[0];
            if (idField.length === 1) {
                lineIdField = idField[0];
            } else {
                lineIdField = idField[1];
            }

            if (detailsSubLists != null) {
                detailsSubLists = detailsSubLists.split(',');
                if (detailsSubLists.length === 0) detailsSubLists = null;
            }

            if (detailsSubLists != null) {
                selectedIds = getSelectedObjects({
                    idField: lineIdField,
                    usedRangeField: usedRangeField,
                    list: detailsList,
                    subLists: detailsSubLists,
                    subListIndex: 0
                });
            } else {
                selectedIds = getSelectedObjects({
                    idField: lineIdField,
                    usedRangeField: usedRangeField,
                    list: detailsList
                });
            }

            showWindow({
                columns: options.columns,
                model: options.model,
                selectedIds: selectedIds,
                selectedIdField: entityIdField,
                selectedIndex: index,
                entities: options.entities,
                allowDuplicates: options.allowDuplicates,
                multiselect: options.multiselect,
                onOkCallback: function (ids) {
                    if (detailsSubLists != null) {
                        setSelectedObjects({
                            idField: lineIdField,
                            index: index,
                            list: detailsList,
                            ids: ids,
                            subLists: detailsSubLists,
                            subListIndex: 0
                        });
                    } else {
                        setSelectedObjects({
                            idField: lineIdField,
                            index: index,
                            list: detailsList,
                            ids: ids
                        });
                    }
                    //console.log(validationFunction);
                    if (validationFunction) {
                        var $parent = null;
                        if (detailsSubLists != null) {
                            for (var i = 0, icnt = detailsSubLists.length; i < icnt; i += 2) {
                                $parent = {
                                    $parent: $parent,
                                    $index: Number(detailsSubLists[i])
                                };
                            }
                        }
                        (new Function('$scope', '$index', '$parent', validationFunction))(options.$scope, index, $parent);
                    }
                    options.$scope.$apply();

                    if (typeof options.onOkCallback === 'function') {
                        options.onOkCallback.call(me, ids);
                    }
                }
            });
        };
    };


    franwell.entitySelector.managePopover = franwell.entitySelector.managePopover || function (selector, entities, entityIdField, entityId, titleField, columns, columnsToSkip, columnsWithHtml) {
        var $target = $(selector),
            entity = null;

        if (entityId != null) {
            for (var i = 0, icnt = entities.length; i < icnt; i++) {
                if (entities[i][entityIdField] !== entityId) continue;
                entity = entities[i];
                break;
            }
        }

        $target.popover('destroy');
        if (entity) {
            if (!columnsToSkip || !(columnsToSkip instanceof Array)) columnsToSkip = [];
            columnsToSkip.push(titleField);

            $target.popover({
                container: 'body',
                placement: 'right',
                trigger: 'hover',
                html: true,
                title: franwell.getRecursiveProperty(entity, titleField),
                content: franwell.formatGridColumnsAsHtml(entity, columns, columnsToSkip, columnsWithHtml)
            });
        }
    };
})();