﻿// franwell.bootstrap.js
// (c) 2012-2015 Franwell, Inc.

// requires - jQuery.js, Bootstrap.min.js

(function () {
    'use strict';


    var root = window,
        franwell = root.franwell = root.franwell || {};
    franwell.$ = root.jQuery;


    // =======================================================================================================
    // restoreTabSelection : On page load, selects the tab (if found) specified by the hash portion of the URL
    // =======================================================================================================

    franwell.restoreTabSelection = franwell.restoreTabSelection || function (defaultTabName, matchHashWithTab) {
        var selectedTab = (location.hash !== '' ? location.hash : defaultTabName),
            $tabButton = $('a[href="' + selectedTab + '"]');

        if ($tabButton.length === 0) {
            $tabButton = $('a[href="' + defaultTabName + '"]');
        }
        $tabButton.tab('show');

        if (matchHashWithTab == null) matchHashWithTab = true;
        if (matchHashWithTab) {
            $('a[href][data-toggle="tab"]').click(function (e) {
                if (this.hash !== '#') {
                    e.preventDefault();
                    if (window.location.hash !== this.hash) {
                        window.location.hash = this.hash;
                    } else {
                        return false;
                    }
                }
                return true;
            });
        }
    };
})();