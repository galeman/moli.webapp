﻿// franwell.angularjs.js
// (c) 2012-2015 Franwell, Inc.

// requires - franwell.js, Angular.min.js

(function () {
    'use strict';


    var root = window,
        franwell = root.franwell = root.franwell || {};
    franwell.ng = franwell.ng || {};


    franwell.ng.globalMethods = franwell.ng.globalMethods || {
        now: function () {
            return new Date();
        },
        oneYearFromDate: function (date) {
            var result;
            if (date instanceof Date) {
                result = date;
                result.setFullYear(result.getFullYear() + 1);
            } else {
                result = null;
            }
            return result;
        },
        // As of AngularUI 0.7.0, the Typeahead directive doesn't unset the input
        // text, if what was typed in doesn't match an entry in its list. This
        // function helps with that.
        // The typeahead directive must be set with typeahead-editable="true",
        // and this function should be called using ng-blur.
        typeaheadValidator: function (entities, entityValueField, line, lineValueField) {
            var value = line[lineValueField],
                valueFound = false;

            for (var i = 0, icnt = entities.length; i < icnt; i++) {
                if (entities[i][entityValueField] !== value) continue;

                valueFound = true;
                break;
            }

            if (!valueFound) {
                line[lineValueField] = null;
            }
        },
        // As of AngularUI 0.7.0, the Typeahead directive doesn't abide by the
        // select format, "value as id for entity in entities". It doesn't display
        // the label of the selected entity, but rather the value portion.
        // This function should be called using typeahead-input-formatter.
        // https://github.com/angular-ui/bootstrap/issues/981
        typeaheadFormatter: function (entities, value, valueField, labelField) {
            var entity,
                result = '';

            for (var i = 0, icnt = entities.length; i < icnt; i++) {
                entity = entities[i];
                if (entity[valueField] !== value) continue;

                result = franwell.getRecursiveProperty(entity, labelField);
                if (result == null || result.trim() === '') result = 'Id # ' + value;
                break;
            }

            return result;
        }
    };


    // This method serves to "acquire" or "update" (or compile) any newly inserted HTML that makes use of AngularJS.
    franwell.ng.recompileDocument = franwell.ng.recompileDocument || function (element) {
        // AngularJS pre-1.2.0 versions:
        // This method ensures that the AngularJS app is started.
        //angular.bootstrap(document, ['globalApp']);

        // AngularJS 1.2.0 and newer:
        element = franwell.$(element || document);

        var $injector = angular.injector(['ng', 'globalApp']);
        $injector.invoke(function ($rootScope, $compile) {
            var scope = $rootScope.$new();
            $compile(element.contents(), true)(scope);
            scope.$digest();
        });
    };


    franwell.ng.getControllerScope = franwell.ng.getControllerScope || function (element) {
        return angular.element(franwell.$(element)).scope();
    };


    window.globalAngularJsApp = angular
        .module('globalApp', ['ui.bootstrap'])
        .config(['$locationProvider', function ($locationProvider) {
            $locationProvider.html5Mode(true).hashPrefix('!');
        }])
        .filter('validateNumber', function () {
            return function (number, valid, invalid, zero, positiveOnly) {
                if (isNaN(parseFloat(number)) || !isFinite(number)) {
                    return invalid;
                } else if (number == 0) {
                    // validating input elements store the value as a String
                    // therefore, this condition must use "==", instead of "==="
                    return zero;
                } else if (positiveOnly && number <= 0) {
                    return invalid;
                } else {
                    return valid;
                }
            };
        })
        // RepeaterCtrl is a generic controller used throughout the application for the ng-repeat directive.
        //
        // There are two ways of passing data, and custom methods into this controller.
        //   1. window.preload.repeaterData is a JavaScript object that contains any data you may require.
        //      - This object will be accessible through $scope.preload.repeaterData.
        //      - This JavaScript object may contain an array named Details.
        //      - The Details array, if present, will be used as the default/initial data for the repeater.
        //   2. window.preload.methods is another JavaScript object that may contain custom helper methods.
        //      - This object will be accessible through $scope.preload.methods.
        //      - These helper methods cannot access the controller scope directly; they can only access the
        //        objects passed through custom method parameters. The other option would be to use the
        //        franwell.ng.getControllerScope function to obtain the $scope of a controller.
        //
        // Under the default functionality, this controller will always keep a minimum of one object in the array/repeater.
        // Calling the scope method "removeLine" on the last item will only reset the item to a blank/empty object.
        //
        // This controller contains a generic $scope method that helps ensure an item is not already selected on another row.
        // TODO: Update this comment documentation
        //   - Method signature: validateSelection(entities, index, line, lineIdField, notification)
        //   - entities: an array of objects that will be searched based on the equivalent property in the "line" parameter object.
        //   - index: simply specify "index" - this is the index of the current row.
        //   - line: the $scope model object that contains at least the "lineIdField" property.
        //   - lineIdField:
        //       - string type: name of the property that will be matched on the $scope row data (or detailsList parameter), entities and line.
        //       - array: must contain a minimum of two elements; the first will be the entity unique identifier (entities array), and the second will be used for $scope row data, and line.
        //   - notification:
        //       - string type: name of the property whose value will be displayed to the user when notified that the newly selected item is already selected elsewhere.
        //       - function: will be called when an existing selection is found.
        //           - the function will be called with the following parameters: entityObject, index, line, existingIndex
        //           - if the return value is null or false, the selection will be prevented; any other value will be assigned to the $scope row.
        //   - detailsList: array which holds the currently selected UI data; if none specified, repeaterLines will be used.
        //
        .controller('RepeaterCtrl', ['$scope', '$window', function ($scope, $window) {
            var getDefaultLine = function (details, defaultLine) {
                if (defaultLine == null) {
                    // If adding to a custom list, we use an empty object.
                    defaultLine = details === $scope.repeaterLines || !details
                        ? $scope.preload.defaults.line
                        : {};
                }
                var result = $.extend(true, {}, defaultLine);
                return result;
            };

            $scope.globalMethods = $scope.globalMethods || $window.franwell.ng.globalMethods;

            $scope.preload = $scope.preload || {};
            $scope.preload.defaults = $window.preload.defaults || {};
            $scope.preload.defaults.line = $scope.preload.defaults.line || {};
            $scope.preload.repeaterData = $window.preload.repeaterData || {};
            $scope.preload.methods = $window.preload.methods || {};

            $scope.preload.repeaterData.Details = $scope.preload.repeaterData.Details || [];
            $scope.repeaterLines = $scope.preload.repeaterData.Details;
            $scope.template = $scope.template || {};

            // methods
            $scope.addLine = function (details, defaultLine) {
                defaultLine = getDefaultLine(details, defaultLine);
                details = details || $scope.repeaterLines;
                details.push(defaultLine);
            };
            $scope.removeLine = function (index, details, keepOneLine, defaultLine) {
                details = details || $scope.repeaterLines;
                if (keepOneLine == null) keepOneLine = true;

                var oneLineLeft = (details.length === 1),
                    confirmText = 'Are you sure you wish to ' + (oneLineLeft && keepOneLine ? 'clear' : 'remove') + ' line # ' + (index + 1) + '?';

                if (!confirm(confirmText)) return;

                details.splice(index, 1);
                if (keepOneLine && oneLineLeft) {
                    $scope.addLine(details, defaultLine);
                }
            };
            $scope.applyTemplatedField = function (templateValue, fieldName, details) {
                details = details || $scope.repeaterLines;
                for (var i = 0, icnt = details.length; i < icnt; i++) {
                    details[i][fieldName] = templateValue;
                }
            };
            $scope.validateSelection = function (lists, selection, idField, displayField, overwriteLine, multiselect, defaultLine) {
                if (overwriteLine == null) overwriteLine = false;
                if (multiselect == null) multiselect = false;

                var entities,
                    detailsList,
                    entityIdField,
                    lineIdField,
                    lineIndex,
                    line,
                    lineIdValue,
                    allSelectedLines = [],
                    selectedEntriesTextIsMultiline,
                    selectedEntityObject = null,
                    duplicateSelectedLines = [],
                    i, icnt,

                    normalizeValue = function (value) {
                        return value instanceof Date
                            ? value.toString()
                            : value;
                    },
                    captureAllSelectedLines = function (list, skipIndex, entryFormatterFunction) {
                        for (var z = 0, zcnt = list.length; z < zcnt; z++) {
                            if (z === skipIndex || !list[z]) continue;
                            allSelectedLines.push({
                                id: normalizeValue(list[z][lineIdField]),
                                position: entryFormatterFunction == null
                                    ? (z + 1).toString()
                                    : entryFormatterFunction(z)
                            });
                        }
                    },
                    captureAllDuplicateSelectedLines = function () {
                        for (var z = 0, zcnt = allSelectedLines.length; z < zcnt; z++) {
                            if (!allSelectedLines[z] || allSelectedLines[z].id !== lineIdValue) continue;
                            duplicateSelectedLines.push(allSelectedLines[z].position);
                        }
                    };


                if (lists[0] instanceof Array && lists[1] instanceof Array) {
                    entities = lists[0];
                    detailsList = lists[1];
                } else {
                    entities = lists;
                    detailsList = $scope.repeaterLines;
                }

                if (idField instanceof Array) {
                    entityIdField = idField[0];
                    lineIdField = idField[1];
                } else {
                    entityIdField = idField;
                    lineIdField = idField;
                }

                if (selection instanceof Array) {
                    var selectionIndex = 0, // selection[][0] = selected index
                        selectionListName = 1, // selection[][1] = list name
                        selectionUiName = 2, // selection[][2] = UI name
                        selectionDisplayField = 3, // selection[][3] = display field

                        getLineDisplay = function (fieldName, displayValue, index) {
                            var result = fieldName + ' # ' + (index + 1).toString();
                            if (displayValue) result += ': ' + displayValue;
                            return result;
                        },
                        populateDuplicateSelectedEntries = function (options) {
                            var list = options.list,
                                subLists = options.subLists,
                                subListIndex = options.subListIndex,
                                allParentsSelected = options.allParentsSelected,
                                parentSelectedText = options.parentSelectedText;

                            if (subListIndex == null) subListIndex = 0;
                            if (allParentsSelected == null) allParentsSelected = true;
                            if (parentSelectedText == null) parentSelectedText = '';

                            if (subLists == null || subListIndex === subLists.length - 1) {
                                var skipIndex;

                                if (allParentsSelected) {
                                    detailsList = list;
                                    lineIndex = subLists[subListIndex];
                                    line = detailsList[lineIndex];
                                    lineIdValue = normalizeValue(line[lineIdField]);

                                    skipIndex = lineIndex;
                                } else {
                                    skipIndex = -1;
                                }

                                captureAllSelectedLines(list, skipIndex, function (index) {
                                    return parentSelectedText +
                                        ' ├' + '─'.repeat(subListIndex) + ' Line # ' + (index + 1).toString() +
                                        ' <-- (selected here)';
                                });
                            } else {
                                var subListEntry = subLists[subListIndex],
                                    nextSubListIndex = subListIndex + 1,
                                    nextParentSelectedDisplay, nextAllParentsSelected, nextParentSelectedText;

                                for (var z = 0, zcnt = list.length; z < zcnt; z++) {
                                    nextParentSelectedDisplay = getLineDisplay(
                                        subListEntry[selectionUiName],
                                        list[z][subListEntry[selectionDisplayField]],
                                        z);
                                    nextAllParentsSelected = allParentsSelected && subListEntry[selectionIndex] === z;
                                    nextParentSelectedText = parentSelectedText +
                                        ' ├' + '─'.repeat(subListIndex) + ' ' + nextParentSelectedDisplay + '\n';

                                    populateDuplicateSelectedEntries({
                                        list: list[z][subListEntry[selectionListName]],
                                        subLists: subLists,
                                        subListIndex: nextSubListIndex,
                                        allParentsSelected: nextAllParentsSelected,
                                        parentSelectedText: nextParentSelectedText
                                    });
                                }
                            }
                        };


                    selectedEntriesTextIsMultiline = true;
                    populateDuplicateSelectedEntries({
                        list: detailsList,
                        subLists: selection
                    });
                } else {
                    lineIndex = selection;
                    line = detailsList[lineIndex];
                    lineIdValue = normalizeValue(line[lineIdField]);
                    selectedEntriesTextIsMultiline = false;
                    captureAllSelectedLines(detailsList, lineIndex);
                }

                if (lineIdValue == null) return;


                for (i = 0, icnt = entities.length; i < icnt; i++) {
                    var entityObject = entities[i],
                        entityIdValue = normalizeValue(entityObject[entityIdField]);
                    if (entityIdValue !== lineIdValue) continue;

                    // Create copy of "line" if it is already selected, because we may modify its properties,
                    // and if properties are modified, we don't want them replicating back to the original
                    // "line" object - the specified index in the "detailsList" is replaced anyway.
                    if (overwriteLine) {
                        var tempLine = $.extend(true, {}, line);
                        // Remove all Array properties because $.extend(true, ...) combines
                        // array properties (whose names match) instead of overwriting them
                        for (var key in tempLine) {
                            if (!(tempLine[key] instanceof Array)) continue;
                            delete tempLine[key];
                        }
                        selectedEntityObject = $.extend(true, tempLine, entityObject);
                    } else {
                        selectedEntityObject = $.extend(true, {}, line);
                        // add/replace the displayField in "selectedEntityObject"
                        // so that the user notifications down below work properly
                        selectedEntityObject[displayField] = entityObject[displayField];
                    }
                    break;
                }

                if (selectedEntityObject == null) return;


                captureAllDuplicateSelectedLines();
                if (duplicateSelectedLines.length !== 0) {
                    var message,
                        resetEntityObject;

                    message = '"' + selectedEntityObject[displayField] + '" is already selected on';
                    if (selectedEntriesTextIsMultiline) {
                        message += ':\n\n' + duplicateSelectedLines.join('\n\n');
                    } else if (duplicateSelectedLines.length === 1) {
                        message += ' line # ' + duplicateSelectedLines[0] + '.';
                    } else {
                        message += ' the following lines:\n\n ├─ ' + duplicateSelectedLines.join('\n ├─ ');
                    }

                    if (multiselect) {
                        message += '\n\nSelect again on this line?';
                        resetEntityObject = !confirm(message);
                    } else {
                        alert(message);
                        resetEntityObject = true;
                    }

                    if (resetEntityObject) {
                        if (overwriteLine) {
                            defaultLine = getDefaultLine(detailsList, defaultLine);
                            selectedEntityObject = defaultLine;
                        } else {
                            selectedEntityObject = line;
                            selectedEntityObject[lineIdField] = null;
                        }
                    }
                }

                detailsList[lineIndex] = selectedEntityObject;
            };

            if ($scope.repeaterLines.length === 0) {
                $scope.addLine();
            }
        }]);
})();