// franwell.kendo.js
// (c) 2012-2015 Franwell, Inc.

// requires - jQuery.js, Kendo.web.js

(function () {
    'use strict';


    var root = window,
        franwell = root.franwell = root.franwell || {},
        toolbarElementIdCounter = 0;
    franwell.$ = franwell.$ || root.jQuery;
    franwell.kendo = franwell.kendo || {};


    // =========================================================
    // kendo.slider : Creates a slider from a text input element
    // =========================================================

    franwell.kendo.slider = franwell.kendo.slider || function ($inputElement, options, changeEventHandler) {
        var sliderOptions = $.extend({
            change: changeEventHandler,
            slide: changeEventHandler
        }, options);
        return $inputElement
            .css('margin-top', '7px')
            .css('margin-bottom', '7px')
            .kendoSlider(sliderOptions);
    };


    // ======================================
    // kendo.toolbarButton : TODO Description
    // ======================================

    franwell.kendo.toolbarButton = franwell.kendo.toolbarButton || function (label, clickHandler) {
        toolbarElementIdCounter++;
        var elementId = 'js-toolbarbutton-' + toolbarElementIdCounter.toString(),
            template = '<button class="btn shadow ' + elementId + '">' + label + '</button>';
        return { id: elementId, template: template, clickHandler: clickHandler };
    };


    // ========================================
    // kendo.toolbarCheckbox : TODO Description
    // ========================================

    franwell.kendo.toolbarCheckbox = franwell.kendo.toolbarCheckbox || function (label, checked, changeHandler) {
        toolbarElementIdCounter++;
        var elementId = 'js-toolbarcheckbox-' + toolbarElementIdCounter.toString(),
            template = '<label class="checkbox inline"><input ' + (checked ? 'checked="checked"' : '') + ' class="' + elementId + '" type="checkbox" />' + label + '</label>';
        return { id: elementId, template: template, changeHandler: changeHandler };
    };


    // =====================================
    // kendo.toolbarRadio : TODO Description
    // =====================================

    franwell.kendo.toolbarRadio = franwell.kendo.toolbarRadio || function (label, name, checked, changeHandler) {
        toolbarElementIdCounter++;
        var elementId = 'js-toolbarradio-' + toolbarElementIdCounter.toString(),
            template = '<label class="radio inline"><input ' + (checked ? 'checked="checked"' : '') + ' class="' + elementId + '" name="' + name + '" type="radio" />' + label + '</label>';
        return { id: elementId, template: template, changeHandler: changeHandler };
    };


    // =======================================
    // kendo.toolbarTextbox : TODO Description
    // =======================================

    franwell.kendo.toolbarTextbox = franwell.kendo.toolbarTextbox || function (label, width, initialValue, changeHandler) {
        toolbarElementIdCounter++;
        var elementId = 'js-toolbartextbox-' + toolbarElementIdCounter.toString(),
            template = '<div class="input-prepend" style="margin-bottom: 0;"><span class="add-on">' + label + '</span><input class="' + elementId + '"' + (width ? ' style="width: ' + width + 'px;"' : '') + ' type="text" value="' + (initialValue || '') + '" /></div>';
        return { id: elementId, template: template, changeHandler: changeHandler };
    };


    // ================================
    // kendo.toolbar : TODO Description
    // ================================

    franwell.kendo.toolbar = franwell.kendo.toolbar || function (toolbarGroups) {
        var template = '<div class="btn-toolbar">';
        franwell.$.each(toolbarGroups, function (groupIndex, group) {
            template += '<div class="btn-group">';
            franwell.$.each(group, function (buttonIndex, button) { template += button.template; });
            template += '</div>';
        });
        template += '</div>';
        return { toolbarGroups: toolbarGroups, template: template };
    };


    // ========================================
    // kendo.toolbarDropdown : TODO Description
    // ========================================

    franwell.kendo.toolbarDropdown = franwell.kendo.toolbarDropdown || function (id, label, links, hashkey) {
        var clickHandler;

        var template = '<div class="btn-group"><a class="btn dropdown-toggle" data-toggle="dropdown" href="\\#">' + label + ' <span class="caret"></span></a><ul class="dropdown-menu">';
        franwell.$.each(links, function (linkIndex, link) {
            clickHandler = link.clickHandler || link.id + 'Handler();';
            template = template + '<li><a href="\\#' + hashkey + '" onClick="' + clickHandler + '">' + link.label + '</a></li>';
        });
        template = template + '</ul></div>';

        var enable = function () {
            franwell.$('#' + id).attr('disabled', false);
        };

        var disable = function () {
            franwell.$('#' + id).attr('disabled', true);
        };

        return { name: id, template: template, enable: enable, disable: disable };
    };


    // =========================================
    // kendo.clientDataSource : TODO Description
    // ----------------------------------------------------------------------
    // Options:
    //  - data : DataSource data
    //  - model : DataSource schema model
    //  - pageSize : number of rows to display
    //  - sort : default sort to apply
    //  - dataChangedHandler : function to handle data change events
    // =========================================

    franwell.kendo.clientDataSource = franwell.kendo.clientDataSource || function (options) {
        return new kendo.data.DataSource({
            data: options.data || [],
            schema: {
                model: options.model,
                type: 'json'
            },
            change: options.dataChangedHandler,
            pageSize: options.pageSize || 20,
            sort: options.sort
        });
    };


    // ======================================================================
    // kendo.serverDataSource : TODO Description
    // ----------------------------------------------------------------------
    // Options:
    //  - url : the URL
    //  - data : data to pass on along with the request
    //  - model : DataSource schema model
    //  - pageSize : number of rows to display
    //  - sort : default sort to apply
    //  - serverAggregates : submit aggregates to be calculated by the server
    //  - serverFiltering : submit filters to the server
    //  - serverGrouping : submit grouping requests to the server
    //  - serverPaging : perform row paging on the server
    //  - serverSorting : perform columng sorting on the server
    //  - dataChangedHandler : function to handle data change events
    // ======================================================================

    franwell.kendo.serverDataSource = franwell.kendo.serverDataSource || function (options) {
        options = options || {};

        return new kendo.data.DataSource({
            transport: {
                read: function (readOptions) {
                    franwell.submitJson({
                        url: options.url || '',
                        method: readOptions.method || 'POST',
                        data: { data: options.data, request: readOptions.data },
                        success: function (result) {
                            readOptions.success(result);
                        },
                        useSpinner: false
                    });
                }
            },
            schema: {
                model: options.model,
                type: 'json',
                data: 'Data',
                errors: 'Errors',
                total: 'Total'
            },
            autoSync: false,
            batch: false,
            change: options.dataChangedHandler,
            pageSize: options.pageSize || 20,
            sort: options.sort,
            serverAggregates: options.serverAggregates == null ? true : options.serverAggregates,
            serverFiltering: options.serverFiltering == null ? true : options.serverFiltering,
            serverGrouping: options.serverGrouping == null ? true : options.serverGrouping,
            serverPaging: options.serverPaging == null ? true : options.serverPaging,
            serverSorting: options.serverSorting == null ? true : options.serverSorting
        });
    };


    // ===============================================
    // franwellGrid : TODO Description
    // -----------------------------------------------
    // Common options:
    //  - dataSource : franwell.kendo.serverDataSource (or clientDataSource)
    //  - columns : []
    //  - toolbar : franwell.kendo.toolbar([[], []])
    //  - selectionChangedHandler : function (e) { }
    //  - dataBoundHandler : function (e) { }
    //  - detailTemplate : ''
    //  - detailInit : function (e) { }
    //  - emptyGridMessage : ''
    // ===============================================

    franwell.$.fn.franwellGrid = franwell.$.fn.franwellGrid || function (options) {
        var defaultOptions = {
            autoBind: false,
            dataSource: {},
            filterable: {
                messages: {
                    isTrue: 'Yes',
                    isFalse: 'No'
                },
                operators: {
                    string: {
                        contains: "Contains",
                        doesnotcontain: "Does not contain",
                        startswith: "Starts with",
                        endswith: "Ends with",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            },
            groupable: false,
            pageable: {
                buttonCount: 10,
                info: true,
                input: true,
                messages: { display: 'Viewing {0} - {1} ({2} total)' },
                numeric: true,
                pageSizes: [5, 10, 20, 50, 100, 500],
                previousNext: true,
                refresh: true
            },
            scrollable: false,
            selectable: 'multiple, row',
            sortable: {
                mode: 'single',
                allowUnsort: false
            }
        };


        options = options || {};
        if (options.toolbar instanceof Object) {
            options.toolbarObject = options.toolbar;
            options.toolbar = options.toolbarObject.template;
        }
        if (options.selectionChangedHandler) {
            options.change = function (e) {
                var items = [],
                    grid = e.sender;
                grid.select().each(function () {
                    var item = grid.dataItem(franwell.$(this));
                    items.push(item);
                });
                options.selectionChangedHandler(items);
            };
        }
        options.dataBound = function (e) {
            var grid = this;
            if (!grid.dataSource || !grid.dataSource._total) {
                if (grid.table.siblings('.grid-no-data').length === 0) {
                    setTimeout(function () {
                        // Introduced a small delay to setting the "Information not found" text,
                        // because sometimes the div would not get added.
                        grid.table.after(
                            '<div class="grid-no-data">' +
                            (options.emptyGridMessage ? options.emptyGridMessage : 'No data available to display') +
                            '</div>');
                    }, 500);
                }
            } else {
                franwell.$('.grid-no-data', grid.element).remove();
            }

            if (options.dataBoundHandler) {
                options.dataBoundHandler(e);
            }
        };


        var gridOptions = franwell.$.extend(true, defaultOptions, options);
        franwell.$(this).each(function () {
            franwell.$(this).kendoGrid(gridOptions);
        });

        if (options.toolbarObject instanceof Object) {
            var toolbarGroups = options.toolbarObject.toolbarGroups,
                toolbarGroup, element,
                i, icnt, x, xcnt;

            for (i = 0, icnt = toolbarGroups.length; i < icnt; i++) {
                toolbarGroup = toolbarGroups[i];
                for (x = 0, xcnt = toolbarGroup.length; x < xcnt; x++) {
                    element = toolbarGroup[x];

                    if (element.clickHandler) {
                        franwell.$('.' + element.id).click(element.clickHandler);
                    }
                    if (element.changeHandler) {
                        franwell.$('.' + element.id).change(element.changeHandler);
                    }
                }
            }
        }
    };


    // ===============================================================================================
    // gridConditionalFormatting : TODO Description
    // -----------------------------------------------------------------------------------------------
    // Options:
    //  - name : name of field in dataSource
    //  - value : value of field
    //  - class : if the row "field" matches "value", then apply this class
    //  - css : if the row "field" matches "value", then apply this jQuery css object
    //  - condition : function that returns true if the row should be formatted.
    //                example: function (fieldValue, value, $element) { return fieldValue == value }
    // ===============================================================================================

    franwell.kendo.gridConditionalFormatting = franwell.kendo.gridConditionalFormatting || function (fields) {
        if (!fields || !fields.length) {
            return null;
        }

        return function (e) {
            var grid = e.sender;
            var gridData = grid.dataSource.view();

            for (var i = 0, icnt = gridData.length; i < icnt; i++) {
                var $currenRow = $(grid.table.find('tr[data-uid="' + gridData[i].uid + '"]'));

                //if the record fits the custom condition
                for (var f = 0, fcnt = fields.length; f < fcnt; f++) {
                    var field = fields[f];

                    if (typeof field.condition === 'function') {
                        field.condition(gridData[i][field.name], field.value, $currenRow);
                    }
                        // use "==" to compare the value, because we don't know what format
                        // the grid may have, nor what format the field value has
                    else if (field.name && field.value && gridData[i][field.name] == field.value) {
                        if (typeof field.cssClass === 'string') {
                            $currenRow.addClass(field.cssClass);
                        }
                        if (typeof field.cssStyles === 'object') {
                            $currenRow.css(field.cssStyles);
                        }
                    }
                }
            }
        };
    };


    // ==================================
    // franwellGridIds : TODO Description
    // ==================================

    franwell.$.fn.franwellGridIds = franwell.$.fn.franwellGridIds || function (idField) {
        var ids = [],
            grid = franwell.$(this).data('kendoGrid');

        idField = idField || 'Id';
        grid.select().each(function () {
            var item = grid.dataItem(franwell.$(this));
            ids.push(item[idField]);
        });

        return ids;
    };


    // =======================================================
    // contentModal : Opens a modal window with static content
    // -------------------------------------------------------
    //  - title : The title of the modal
    //  - content : The static content
    // =======================================================

    franwell.contentModal = franwell.contentModal = function (title, content) {
        title = title || '';
        content = content || '';

        var element, window;

        element = franwell.$(document.createElement('div'));
        element.kendoWindow({
            actions: ['Close'],
            draggable: true,
            modal: true,
            resizable: false,
            visible: false
        });
        window = element.data('kendoWindow');

        element.html(content);
        window.title(title);
        window.center();
        window.open();
    };


    // ==================================================================================
    // gridModal : Opens a modal window with a form and reloads the grid after submission
    // ----------------------------------------------------------------------------------
    //  - title : The title of the modal
    //  - formUrl : The URL of the form
    //  - formQuery : Query parameters for the form URL
    //  - formSelector : Selector for the form
    //  - submitUrl : The URL to submit to via AJAX
    //  - $grid : jQuery of grid, or array of grids, to be refreshed after closing the modal
    // ==================================================================================

    franwell.kendo.modalWindow = franwell.kendo.modalWindow || function (title, formUrl, formQuery, formSelector, submitUrl, $grid, success, openHandler, requestType) {
        title = title || '';
        formUrl = formUrl || '';
        formQuery = formQuery || {};
        formSelector = formSelector || 'form';
        submitUrl = submitUrl || '';
        success = success || function () { };
        requestType = requestType || 'GET';

        var usingSpinner = franwell.startSpinner(),
            $element = franwell.$(document.createElement('div')),
            refreshHandler = function () {
                var me = this;

                $element.find(formSelector).franwellActivateAjaxForm({
                    url: submitUrl,
                    type: 'POST',
                    success: function (e) {
                        me.close();
                        if ($grid instanceof Array) {
                            for (var i = 0, icnt = $grid.length; i < icnt; i++) {
                                $grid[i].data('kendoGrid').dataSource.read();
                            }
                        }
                        else if ($grid) {
                            $grid.data('kendoGrid').dataSource.read();
                        }
                        success(e);
                    }
                });
                $element.find('#cancel').click(function () {
                    me.close();
                });

                me.center();
                me.open();
            };

        $element.kendoWindow({
            // config
            actions: ['Close'],
            draggable: true,
            modal: true,
            resizable: false,
            title: title,
            // events
            open: openHandler,
            activate: function () {
                if (usingSpinner) {
                    franwell.stopSpinner();
                }

                franwell.$(formSelector)
                    .find('input[type=text]:not([readonly]),textarea:not([readonly]),select')
                    .filter(':visible:enabled:first')
                    .focus(); // Set focus to first enabled and visible input in form
            },
            refresh: refreshHandler,
            close: function () {
                setTimeout(function () {
                    $element.data('kendoWindow').destroy();
                }, 1000);
            },
            error: function (e) {
                franwell.errorResponseHandler(e.xhr);
            }
        });

        var kWindow = $element.data('kendoWindow');
        kWindow.refresh({
            url: formUrl,
            data: formQuery,
            traditional: true,
            type: requestType
        });

        return $element;
    };
})();