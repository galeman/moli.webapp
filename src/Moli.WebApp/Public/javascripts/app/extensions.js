﻿(function () {
    Array.prototype.distinct = function () {
        var prims = { "boolean": {}, "number": {}, "string": {} }, objs = [];

        return this.filter(function (item) {
            var type = typeof item;
            if (type in prims)
                return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
            else
                return objs.indexOf(item) >= 0 ? false : objs.push(item);
        });
    };

    String.prototype.repeat = function (count) {
        // http://stackoverflow.com/a/5450113/238722
        if (count < 1) return '';
        var result = '', pattern = this.valueOf();
        while (count > 0) {
            if (count & 1) result += pattern;
            count >>= 1, pattern += pattern;
        }
        return result;
    };
})();