﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Moli.Deployments.Demo;
using Moli.Services;
using Moli.Web.Infrastructure;
using Moli.Web.Infrastructure.Controllers;
using Moli.Web.Infrastructure.Security;
using System;
using System.Diagnostics;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace Moli.WebApp.Controllers.Mvc
{
    [RoutePrefix("deployment")]
    public partial class DeploymentController : BaseMvcController<UserPrincipal>
    {
        private RouteValueDictionary _FrontPageRouteValues;


        protected override RouteValueDictionary FrontPageRouteValues
        {
            get
            {
                return _FrontPageRouteValues ??
                       (_FrontPageRouteValues = MVC.Sessions.FrontPage().GetT4MVCResult().RouteValueDictionary);
            }
        }


        #region // MVC Methods
        // ==================================================

        [GET("migrate")]
        public virtual ActionResult Migrate()
        {
            var startTime = DateTime.Now;
            var stopwatch = Stopwatch.StartNew();

            Deployments.Deployment.MigrateViewDatabase(ConnectionStrings.ViewStore);

            stopwatch.Stop();
            var endTime = DateTime.Now;

            var result = new StringBuilder();
            result.AppendFormat("(Migrate) Started at {0}", startTime);
            result.AppendLine("<hr>");
            result.AppendFormat("(Migrate) Ended at {0}<br />", endTime);
            result.AppendFormat("(Migrate) lasted {0:N0} ms to execute.<br />", stopwatch.ElapsedMilliseconds);

            return Content(result.ToString());
        }

        [GET("reset/clean")]
        public virtual ActionResult CleanReset()
        {
            if (ApplicationEnvironment.IsRunningProduction)
            {
                return HttpNotFound();
            }

            var startTime = DateTime.Now;
            var stopwatch = Stopwatch.StartNew();

            Deployments.Deployment.CleanViewDatabase(ConnectionStrings.ViewStore);
            Deployments.Deployment.MigrateViewDatabase(ConnectionStrings.ViewStore);
            BaseData.Setup();

            stopwatch.Stop();
            var endTime = DateTime.Now;

            var result = new StringBuilder();
            result.AppendFormat("(CleanReset) Started at {0}", startTime);
            result.AppendLine("<hr />");
            result.AppendFormat("(CleanReset) Ended at {0}<br />", endTime);
            result.AppendFormat("(CleanReset) lasted {0:N0} ms to execute.<br />", stopwatch.ElapsedMilliseconds);

            return Content(result.ToString());
        }

        [GET("reset/demo")]
        public virtual ActionResult DemoReset()
        {
            if (ApplicationEnvironment.IsRunningProduction)
            {
                return HttpNotFound();
            }

            var startTime = DateTime.Now;
            var stopwatch = Stopwatch.StartNew();

            Deployments.Deployment.CleanViewDatabase(ConnectionStrings.ViewStore);
            Deployments.Deployment.MigrateViewDatabase(ConnectionStrings.ViewStore);
            DemoData.Setup();

            stopwatch.Stop();
            var endTime = DateTime.Now;

            var result = new StringBuilder();
            result.AppendFormat("(DemoReset) Started at {0}", startTime);
            result.AppendLine("<hr />");
            result.AppendFormat("(DemoReset) Ended at {0}<br />", endTime);
            result.AppendFormat("(DemoReset) lasted {0:N0} ms to execute.<br />", stopwatch.ElapsedMilliseconds);

            return Content(result.ToString());
        }

        [GET("work")]
        public virtual ActionResult Work()
        {
            if (ApplicationEnvironment.IsRunningProduction)
            {
                return HttpNotFound();
            }

            var startTime = DateTime.Now;
            var stopwatch = Stopwatch.StartNew();

            var developer = Repositories.ForUsers.Value.GetUserIdByUsername("franwell-dev");
            Deployments.Demo.Helper.CreateUser(
                "demo",
                "Demo User",
                "Moli@Franwell.com",
                new[] { Permissions.User.FriendlyViewDashboard },
                developer);

            stopwatch.Stop();
            var endTime = DateTime.Now;

            var result = new StringBuilder();
            result.AppendFormat("(DemoReset) Started at {0}", startTime);
            result.AppendLine("<hr />");
            result.AppendFormat("(DemoReset) Ended at {0}<br />", endTime);
            result.AppendFormat("(DemoReset) lasted {0:N0} ms to execute.<br />", stopwatch.ElapsedMilliseconds);

            return Content(result.ToString());
        }

        // ==================================================
        #endregion
    }
}