﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Moli.Web.Infrastructure.Security;
using Moli.WebApp.Infrastructure.Controllers;
using System.Web.Mvc;

namespace Moli.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("dashboard")]
    public partial class DashboardController : AppMvcController
    {
        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            return View();
        }

        // ==================================================
        #endregion
    }
}