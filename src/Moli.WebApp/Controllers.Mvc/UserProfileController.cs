﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Moli.Services;
using Moli.Services.User;
using Moli.Web.Infrastructure.Filters;
using Moli.Web.Infrastructure.Security;
using Moli.WebApp.Infrastructure.Controllers;
using Moli.WebApp.Models;
using System;
using System.Web.Mvc;

namespace Moli.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("user/profile")]
    public partial class UserProfileController : AppMvcController
    {
        private static Lazy<UserRepository> _UserRepository;


        #region // Constructor + DI
        // ==================================================

        static UserProfileController()
        {
            Inject(Repositories.ForUsers);
        }

        private static void Inject(Lazy<UserRepository> dependency)
        {
            _UserRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            var user = _UserRepository.Value.GetAuthorizationDetailsById(User.Id);
            var model = new UserProfileModel
            {
                Username = user.Username,
                FullName = user.Name,
                Email = user.Email,
                SecurityQuestion = user.SecurityQuestion,
                SecurityAnswer = user.SecurityAnswer,

                MustSetPassword = User.LogInState.HasFlag(LogInState.MustSetPassword)
            };
            return View(model);
        }

        // ==================================================
        #endregion
    }
}