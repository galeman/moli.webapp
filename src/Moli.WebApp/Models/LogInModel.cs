using System.Web.Mvc;

namespace Moli.WebApp.Models
{
    public sealed class LogInModel
    {
        public string LogIn { get; set; }
        [AllowHtml]
        public string Password { get; set; }
    }
}