﻿using Moli.Services.Common;
using Moli.Services.Location;
using System;
using System.Collections.Generic;

namespace Moli.WebApp.Models
{
    public class AddEditLocationsModel
    {
        public Boolean Adding { get; set; }
        public IEnumerable<LocationDocument> Locations { get; set; }
        public IEnumerable<AddEditLocationsDetailsModel> Details { get; set; }
    }

    public class AddEditLocationsDetailsModel
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public AxisDirection? AxisDirection { get; set; }
        public OriginLocation? OriginLocation { get; set; }
        public Guid ApiKey { get; set; }
        public Boolean GenerateNewApiKey { get; set; }
    }
}