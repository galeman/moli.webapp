﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using Moli.Services;
using Moli.Services.User;
using Moli.Web.Infrastructure.Filters;
using Moli.Web.Infrastructure.Security;
using Moli.WebApp.Infrastructure.Controllers;
using Moli.WebApp.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Moli.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/users")]
    public sealed class UsersApiController : AppApiController
    {
        private static Lazy<UserRepository> _UserRepository;


        #region // Constructor + DI
        // ==================================================

        static UsersApiController()
        {
            Inject(Repositories.ForUsers);
        }

        private static void Inject(Lazy<UserRepository> dependency)
        {
            _UserRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [POST("updateuserprofile"), HttpPost]
        public HttpResponseMessage UpdateUserProfile(UserProfileModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw UnauthorizedException();
            }


            var logInState = User.LogInState;

            if (!String.IsNullOrWhiteSpace(model.Password) &&
                String.Equals(model.Password, model.PasswordConfirmation, StringComparison.CurrentCulture))
            {
                UserService.SetPassword(User.Id, model.Password);
                logInState = LogInStateFilter.ModifyLogInState(logInState, statesToRemove: LogInState.MustSetPassword);
            }
            if (!String.IsNullOrWhiteSpace(model.Email))
            {
                UserService.SetEmail(User.Id, model.Email);
            }
            if (!String.IsNullOrWhiteSpace(model.SecurityQuestion) &&
                !String.IsNullOrWhiteSpace(model.SecurityAnswer))
            {
                UserService.SetSecurityQuestionAndAnswer(
                    User.Id,
                    model.SecurityQuestion,
                    model.SecurityAnswer);
            }

            UserSession.Update(logInState);

            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        // ==================================================
        #endregion
    }
}