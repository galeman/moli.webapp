﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using Moli.Services;
using Moli.Services.Common;
using Moli.Services.Event;
using Moli.Services.Location;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Moli.WebApp.Controllers.Api.ExternalServices
{
    [RoutePrefix("jobservice")]
    public sealed class JobServiceApiController : ApiController
    {
        private static Lazy<EventRepository> _EventRepository;
        private static Lazy<LocationRepository> _LocationRepository;

        #region // Constructor + DI
        // ==================================================

        static JobServiceApiController()
        {
            Inject(Repositories.ForEvents);
            Inject(Repositories.ForLocations);
        }

        private static void Inject(Lazy<EventRepository> dependency)
        {
            _EventRepository = dependency;
        }

        private static void Inject(Lazy<LocationRepository> dependency)
        {
            _LocationRepository = dependency;
        }
        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        // ==================================================
        #endregion
    }
}