﻿using Moli.Web.Infrastructure.Controllers;
using Moli.Web.Infrastructure.Security;
using System.Web.Mvc;
using System.Web.Routing;

namespace Moli.WebApp.Infrastructure.Controllers
{
    public abstract class AppMvcController : BaseMvcController<UserPrincipal>
    {
        private RouteValueDictionary _FrontPageRouteValues;

        protected override RouteValueDictionary FrontPageRouteValues
        {
            get
            {
                return _FrontPageRouteValues ??
                       (_FrontPageRouteValues = MVC.Sessions.FrontPage().GetT4MVCResult().RouteValueDictionary);
            }
        }
    }
}