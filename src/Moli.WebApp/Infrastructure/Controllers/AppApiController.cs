using Moli.Web.Infrastructure.Controllers;
using Moli.Web.Infrastructure.Security;

namespace Moli.WebApp.Infrastructure.Controllers
{
    [ApiValidateAntiForgeryToken]
    public abstract class AppApiController : BaseApiController<UserPrincipal>
    {
    }
}