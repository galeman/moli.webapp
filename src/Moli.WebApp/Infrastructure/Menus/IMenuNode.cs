﻿using System;
using HtmlTags;

namespace Moli.WebApp.Infrastructure.Menus
{
    public interface IMenuNode
    {
        String PrependHtml { get; }
        String AppendHtml { get; }
        Boolean Selected { get; }
        Boolean Visible { get; }
        HtmlTag ToHtmlTag();
    }
}