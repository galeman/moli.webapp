﻿using System;

namespace Moli.JobService.Infrastructure
{
    public interface IJobEx //: IJob
    {
        Boolean ContinueProcessing { get; set; }
        void Execute();
    }
}